//this hell line keeps board seeds persistent somehow
(function(j,i,g,m,k,n,o){function q(b){var e,f,a=this,c=b.length,d=0,h=a.i=a.j=a.m=0;a.S=[];a.c=[];for(c||(b=[c++]);d<g;) a.S[d]=d++;for(d=0;d<g;d++) e=a.S[d],h=h+e+b[d%c]&g-1,f=a.S[h],a.S[d]=f,a.S[h]=e;a.g=function(b){var c=a.S,d=a.i+1&g-1,e=c[d],f=a.j+e&g-1,h=c[f];c[d]=h;c[f]=e;for(var i=c[e+h&g-1];--b;) d=d+1&g-1,e=c[d],f=f+e&g-1,h=c[f],c[d]=h,c[f]=e,i=i*g+c[e+h&g-1];a.i=d;a.j=f;return i};a.g(g)}function p(b,e,f,a,c){f=[];c=typeof b;if(e&&c=="object") for(a in b) if(a.indexOf("S")<5) try{f.push(p(b[a],e-1))}catch(d){}return f.length?f:b+(c!="string"?"\0":"")}function l(b,e,f,a){b+="";for(a=f=0;a<b.length;a++){var c=e,d=a&g-1,h=(f^=e[a&g-1]*19)+b.charCodeAt(a);c[d]=h&g-1}b="";for(a in e) b+=String.fromCharCode(e[a]);return b}i.seedrandom=function(b,e){var f=[],a;b=l(p(e?[b,j]:arguments.length?b:[(new Date).getTime(),j,window],3),f);a=new q(f);l(a.S,j);i.random=function(){for(var c=a.g(m),d=o,b=0;c<k;) c=(c+b)*g,d*=g,b=a.g(1);for(;c>=n;) c/=2,d/=2,b>>>=1;return(c+b)/d};return b};o=i.pow(g,m);k=i.pow(2,k);n=k*2;l(i.random(),j)})([],Math,256,6,52);

function bingo() {

    //establish board size as 5x5
    var size = 5;

    //look for seed # in url
    function lookup(name) {
        name = name.replace(/[\[]/, "///[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null) {
            return "";
        } else {
            return results[1];
        }
    }

    //determine bingo list based on category
    var bingoList = [];
    var bingoName = lookup('cat');
    var catLen;
    switch (bingoName) {
        case 'BOTWS':
            bingoList = bingoListBOTWS;
            catLen = 5;
            break;
        case 'DKS':
            bingoList = bingoListDKS;
            catLen = 3;
            break;
        case 'DKS2':
            bingoList = bingoListDKS2;
            catLen = 4;
            break;
        case 'DKS3':
            bingoList = bingoListDKS3;
            catLen = 4;
            break;
        case 'DMS':
            bingoList = bingoListDMS;
            catLen = 3;
            break;
        case 'MP':
            bingoList = bingoListMP;
            catLen = 2;
            break;
        case 'RE4':
            bingoList = bingoListRE4;
            catLen = 3;
            break;
    }
    
    //pull seed from url or establish, then continue
    var SEED = lookup('seed');
    SEED = SEED.slice(0,(SEED.length-5-catLen));

    //invoke hell line
    Math.seedrandom(SEED);
    
    //'what squares make lines with me?' reference 
    var lineCheckList = [];
    
    if (size == 5) {
        lineCheckList[1]  = [1,2,3,4,5,10,15,20,6,12,18,24];
        lineCheckList[2]  = [0,2,3,4,6,11,16,21];
        lineCheckList[3]  = [0,1,3,4,7,12,17,22];
        lineCheckList[4]  = [0,1,2,4,8,13,18,23];
        lineCheckList[5]  = [0,1,2,3,8,12,16,20,9,14,19,24];
        
        lineCheckList[6]  = [0,10,15,20,6,7,8,9];
        lineCheckList[7]  = [0,12,18,24,5,7,8,9,1,11,16,21];
        lineCheckList[8]  = [5,6,8,9,2,12,17,22];
        lineCheckList[9]  = [4,12,16,20,9,7,6,5,3,13,18,23];
        lineCheckList[10]  = [4,14,19,24,8,7,6,5];
        
        lineCheckList[11] = [0,5,15,20,11,12,13,14];
        lineCheckList[12] = [1,6,16,21,10,12,13,14];
        lineCheckList[13] = [0,6,12,18,24,20,16,8,4,2,7,17,22,10,11,13,14];
        lineCheckList[14] = [3,8,18,23,10,11,12,14];
        lineCheckList[15] = [4,9,19,24,10,11,12,13];
        
        lineCheckList[16] = [0,5,10,20,16,17,18,19];
        lineCheckList[17] = [15,17,18,19,1,6,11,21,20,12,8,4];
        lineCheckList[18] = [15,16,18,19,2,7,12,22];
        lineCheckList[19] = [15,16,17,19,23,13,8,3,24,12,6,0];
        lineCheckList[20] = [4,9,14,24,15,16,17,18];
        
        lineCheckList[21] = [0,5,10,15,16,12,8,4,21,22,23,24];
        lineCheckList[22] = [20,22,23,24,1,6,11,16];
        lineCheckList[23] = [2,7,12,17,20,21,23,24];
        lineCheckList[24] = [20,21,22,24,3,8,13,18];
        lineCheckList[25] = [0,6,12,18,20,21,22,23,19,14,9,4];
    }
    
    //establish box highlight toggles
    const boxes = document.querySelectorAll("td");
    for (i = 0; i < boxes.length; i++) {
        boxes[i].addEventListener('click', function() {
        if (this.classList.length == 0) {
            this.classList.add("greensquare");
        } else if (this.classList.contains("greensquare")) {
            this.classList.add("redsquare");
            this.classList.remove("greensquare");
        } else if (this.classList.contains("redsquare")) {
            this.classList.remove("redsquare");
        }
        })
    }
    
    //create the magic square
    function difficulty(i) {
        //make a random array 0-4 using 1s, 10s, and 100s digits
        var Num3 = SEED%1000;
        
        var Rem8 = Num3%8;
        var Rem4 = Math.floor(Rem8/2);
        var Rem2 = Rem8%2;
        var Rem5 = Num3%5;
        var Rem3 = Num3%3;
        var RemT = Math.floor(Num3/120);
        
        var Table5 = [0];
        Table5.splice(Rem2, 0, 1);
        Table5.splice(Rem3, 0, 2);
        Table5.splice(Rem4, 0, 3);
        Table5.splice(Rem5, 0, 4);
        
        //then again with the 1000s, 10000s, 100000s digits
        Num3 = Math.floor(SEED/1000);
        Num3 = Num3%1000;
        
        Rem8 = Num3%8;
        Rem4 = Math.floor(Rem8/2);
        Rem2 = Rem8%2;
        Rem5 = Num3%5;
        Rem3 = Num3%3;
        RemT = RemT * 8 + Math.floor(Num3/120);
        
        var Table1 = [0];
        Table1.splice(Rem2, 0, 1);
        Table1.splice(Rem3, 0, 2);
        Table1.splice(Rem4, 0, 3);
        Table1.splice(Rem5, 0, 4);
        
        i--;
        
        //using these tables, determine positioning within magic square
        RemT = RemT%5;
        x = (i+RemT)%5;
        y = Math.floor(i/5);
        
        var e5 = Table5[(x + 3*y)%5];
        var e1 = Table1[(3*x + y)%5];
        
        value = 5*e5 + e1;
        value++;
        return value;
    }
    
    //checks how similar different tasks are to each other based on types
    function checkLine (i, typesA) {
        var synergy = 0;
        for (var j=0; j<lineCheckList[i].length; j++) {
            var typesB = bingoBoard[lineCheckList[i][j]+1].types;
            if (typeof typesB != 'undefined') {
                for (var k=0; k < typesA.length; k++) {
                    for (var l=0; l < typesB.length; l++) {
                        if (typesA[k] == typesB[l]) {
                            synergy++;
                            if (k==0) { synergy++ };
                            if (l==0) { synergy++ };
                        }
                    }
                }
            }
        }
        return synergy;
    }
    
    //this takes category # from function above and stores it in a square
    var bingoBoard = [];
    for (var i=1;i<=25;i++) {
        bingoBoard[i] = {difficulty: difficulty(i)};
    }
    
    //populate the bingo board in the array
    for (var i=1; i<=25; i++) {
        var getDifficulty = bingoBoard[i].difficulty;
        var RNG = Math.floor(bingoList[getDifficulty].length * Math.random());
        var j = 0, synergy = 0, currentObj = null, minSynObj = null;
        
        //minimize synergy to reduce same-y tasks in the same row
        do {
        currentObj = bingoList[getDifficulty][(j+RNG)%bingoList[getDifficulty].length];
        synergy = checkLine(i, currentObj.types);
        if (minSynObj == null || synergy < minSynObj.synergy) {
            minSynObj = { synergy: synergy, value: currentObj };
        };
        j++;
        } while ((synergy != 0) && (j<bingoList[getDifficulty].length));
        
        bingoBoard[i].types = minSynObj.value.types;
        bingoBoard[i].name = minSynObj.value.name;
        bingoBoard[i].synergy = minSynObj.synergy;
    }
    
    //populate the actual table on the page
    for (i=1; i<=25; i++) {
        document.querySelector('#slot'+i).append(bingoBoard[i].name);
    }
}