//establish array variables
//jump to objective list using -OBJ suffix
//for example: DMS-OBJ, RE4-OBJ
var bingoListBOTWS = [];
var bingoListDKS = [];
var bingoListDKS2 = [];
var bingoListDKS3 = [];
var bingoListDMS = [];
var bingoListMP = [];
var bingoListRE4 = [];

//BOTWS-OBJ (breath of the wild shrines)
bingoListBOTWS[1] = [
    { name: "Dah Kaso", types: ["central"] },
    { name: "Rota Ooh", types: ["central"] },
    { name: "Bosh Kala", types: ["dueling"] },
    { name: "Jee Noh", types: ["wasteland"] }
];
bingoListBOTWS[2] = [
    { name: "Kaam Ya'tak", types: ["central"] },
    { name: "Joloo Nah", types: ["highlands"] },
    { name: "Ya Naga", types: ["lake"] },
    { name: "Katah Chuki", types: ["central"] }
];
bingoListBOTWS[3] = [
    { name: "Wahgo Katta", types: ["central"] },
    { name: "Dako Tah", types: ["wasteland"] },
    { name: "Kay Noh", types: ["wasteland"] },
    { name: "Pumaag Nitae", types: ["lake"] }
];
bingoListBOTWS[4] = [
    { name: "Hila Rao", types: ["dueling"] },
    { name: "Ree Dahee", types: ["dueling"] },
    { name: "Shee Vaneer", types: ["dueling"] },
    { name: "Shee Venath", types: ["dueling"] }
];
bingoListBOTWS[5] = [
    { name: "Kuh Takkar", types: ["highlands"] },
    { name: "Dila Maag", types: ["wasteland"] },
    { name: "Korsh O'hu", types: ["wasteland"] },
    { name: "Suma Sahma", types: ["wasteland"] },
    { name: "Ishto Soh", types: ["lake"] }
];
bingoListBOTWS[6] = [
    { name: "Mogg Latan", types: ["ridgeland"] },
    { name: "Sheem Dagoze", types: ["ridgeland"] },
    { name: "Nova Neha", types: ["central"] },
    { name: "Ka'o Makagh", types: ["lake"] },
    { name: "Soh Kofi", types: ["lanayru"] }
];
bingoListBOTWS[7] = [
    { name: "Ha Dahamar", types: ["dueling"] },
    { name: "Ta'loh Naeg", types: ["dueling"] },
    { name: "Toto Sah", types: ["dueling"] },
    { name: "Monya Toma", types: ["forest"] },
    { name: "Daka Tuss", types: ["lanayru"] }
];
bingoListBOTWS[8] = [
    { name: "Dago Chisay", types: ["wasteland"] },
    { name: "Kaya Wan", types: ["lanayru"] },
    { name: "Shae Lova", types: ["ridgeland"] },
    { name: "Toh Yahsa", types: ["ridgeland"] },
    { name: "Zalta Wa", types: ["ridgeland"] }
];
bingoListBOTWS[9] = [
    { name: "Oukah Nata", types: ["faron"] },
    { name: "Shai Utoh", types: ["faron"] },
    { name: "Shoda Sah", types: ["faron"] },
    { name: "Tawa Jinn", types: ["faron"] },
    { name: "Shae Katha", types: ["lake"] }
];
bingoListBOTWS[10] = [
    { name: "Namika Ozz", types: ["central"] },
    { name: "Lakna Rokee", types: ["dueling"] },
    { name: "Sasa Kai", types: ["highlands"] },
    { name: "Sho Dantu", types: ["highlands"] },
    { name: "Mijah Rokee", types: ["ridgeland"] }
];
bingoListBOTWS[11] = [
    { name: "Kema Zoos", types: ["wasteland"] },
    { name: "Dow Na'eh", types: ["hateno"] },
    { name: "Kam Urog", types: ["hateno"] },
    { name: "Mezza Lo", types: ["hateno"] },
    { name: "Shoga Tatone", types: ["lake"] }
];
bingoListBOTWS[12] = [
    { name: "Keeha Yoog", types: ["highlands"] },
    { name: "Misae Suma", types: ["wasteland"] },
    { name: "Raqa Zunzo", types: ["wasteland"] },
    { name: "Tho Kayu", types: ["wasteland"] },
    { name: "Sheh Rata", types: ["lanayru"] }
];
bingoListBOTWS[13] = [
    { name: "Saas Ko'sah", types: ["central"] },
    { name: "Kema Kosassa", types: ["highlands"] },
    { name: "Tena Ko'sah", types: ["tabantha"] },
    { name: "Muwo Jeem", types: ["faron"] },
    { name: "Hawa Koth", types: ["wasteland"] }
];
bingoListBOTWS[14] = [
    { name: "Ze Kasho", types: ["akkala"] },
    { name: "Daga Koh", types: ["eldin"] },
    { name: "Mo'a Keet", types: ["eldin"] },
    { name: "Shae Mo'sah", types: ["eldin"] },
    { name: "Kah Yah", types: ["faron"] }
];
bingoListBOTWS[15] = [
    { name: "Yah Rin", types: ["faron"] },
    { name: "Daag Chokah", types: ["forest"] },
    { name: "Mirro Shaz", types: ["forest"] },
    { name: "Chaas Oeta", types: ["hateno"] },
    { name: "Akh Va'quot", types: ["tabantha"] }
];
bingoListBOTWS[16] = [
    { name: "Qua Raym", types: ["eldin"] },
    { name: "Tah Muhi", types: ["eldin"] },
    { name: "Keo Ruug", types: ["forest"] },
    { name: "Ketoh Wawai", types: ["forest"] },
    { name: "Dunba Taag", types: ["hebra"] }
];
bingoListBOTWS[17] = [
    { name: "Kuhn Sidajj", types: ["forest"] },
    { name: "Gee Ha'rah", types: ["hebra"] },
    { name: "Lanno Kooh", types: ["hebra"] },
    { name: "Ne'ez Yohma", types: ["lanayru"] },
    { name: "Sha Warvo", types: ["tabantha"] }
];
bingoListBOTWS[18] = [
    { name: "Sah Dahaj", types: ["eldin"] },
    { name: "Maag Halan", types: ["forest"] },
    { name: "Myahm Agana", types: ["hateno"] },
    { name: "Mozo Shenno", types: ["hebra"] },
    { name: "Rin Oyaa", types: ["hebra"] }
];
bingoListBOTWS[19] = [
    { name: "Kayra Mah", types: ["eldin"] },
    { name: "Rona Kachta", types: ["forest"] },
    { name: "Qaza Tokki", types: ["hebra"] },
    { name: "Sha Gehma", types: ["hebra"] },
    { name: "Shai Yota", types: ["lanayru"] }
];
bingoListBOTWS[20] = [
    { name: "Shora Hah", types: ["eldin"] },
    { name: "Jitan Sa'mi", types: ["hateno"] },
    { name: "Dagah Keek", types: ["lanayru"] },
    { name: "Bareeda Naag", types: ["tabantha"] },
    { name: "Voo Lota", types: ["tabantha"] }
];
bingoListBOTWS[21] = [
    { name: "Tahno O'ah", types: ["hateno"] },
    { name: "Maka Rah", types: ["hebra"] },
    { name: "Shada Naw", types: ["hebra"] },
    { name: "Kah Okeo", types: ["tabantha"] }
];
bingoListBOTWS[22] = [
    { name: "Goma Assagh", types: ["hebra"] },
    { name: "Rok Uwog", types: ["hebra"] },
    { name: "Rucco Maag", types: ["lanayru"] },
    { name: "Maag No'rah", types: ["ridgeland"] }
];
bingoListBOTWS[23] = [
    { name: "Dah Hesho", types: ["akkala"] },
    { name: "Zuna Kai", types: ["akkala"] },
    { name: "Korgu Chideh", types: ["faron"] },
    { name: "To Quomo", types: ["hebra"] }
]; 
bingoListBOTWS[24] = [
    { name: "Ke'nai Shakah", types: ["akkala"] },
    { name: "Tu Ka'loh", types: ["akkala"] },
    { name: "Gorae Torr", types: ["eldin"] },
    { name: "Kah Mael", types: ["lanayru"] }
];
bingoListBOTWS[25] = [
    { name: "Katosa Aug", types: ["akkala"] },
    { name: "Ritaag Zumo", types: ["akkala"] },
    { name: "Tutsuwa Nima", types: ["akkala"] },
    { name: "Hia Miu", types: ["hebra"] }
];

//DKS-OBJ (dark souls)
//starting class & gift
bingoListDKS[1] = [
    { name: "Pick Bandit class", types: ["startingclass","restrict_axe"] },
    { name: "Pick Knight class", types: ["startingclass","restrict_sword"] },
    { name: "Pick Hunter class", types: ["startingclass","restrict_sword"] },
    { name: "Pick Pyromancer class", types: ["startingclass","restrict_axe"] },
    { name: "Pick Deprived class", types: ["startingclass"] },
    { name: "Pick Wanderer class", types: ["startingclass"] },
    { name: "Pick Thief class", types: ["startingclass","restrict_key"] },
    { name: "Pick Warrior class", types: ["startingclass","restrict_sword"] },
    { name: "Pick Cleric class", types: ["startingclass"] },
    { name: "Pick Sorceror class", types: ["startingclass"] },
    { name: "No Master Key", types: ["startingitem","restrict_key"] },
    { name: "Take Pendant as gift", types: ["startingitem","restrict_key"] },
    { name: "Take Binoculars as gift", types: ["startingitem","restrict_key"] },
    { name: "Take Old Witch's Ring as gift", types: ["startingitem","restrict_key"] }
];
//restrictions (easy)
bingoListDKS[2] = [
    { name: "Right-hand wielding only", types: ["restriction","wielding"] },
    { name: "Left-hand wielding only", types: ["restriction","wielding"] },
    { name: "No swords", types: ["restriction","restrict_weapons"] },
    { name: "No shields", types: ["restriction","restrict_weapons"] },
    { name: "No polearms", types: ["restriction","restrict_weapons"] },
    { name: "Daggers only", types: ["restriction","restrict_weapons"] },
    { name: "Axes only", types: ["restriction","restrict_weapons"] },
    { name: "No rings", types: ["restriction","restrict_armour"] },
    { name: "Starting armour only", types: ["restriction","restrict_armour"] },
    { name: "Starting weapons only", types: ["restriction","restrict_weapons"] }
];
//firelink & township
bingoListDKS[3] = [
    { name: "Pick up Zweihander", types: ["firelink","items"] },
    { name: "Obtain Drake Sword", types: ["township","items"] },
    { name: "Pick up Light Crossbow", types: ["township","items"] },
    { name: "Kick ladder to Township bonfire", types: ["township","shortcut"] }
];
//undead parish
bingoListDKS[4] = [
    { name: "Summon Lautrec (Parish)", types: ["npcs","parish"] },
    { name: "Pick up Fire Keeper Soul (Parish)", types: ["humanity","parish"] },
    { name: "Kill Armoured Boar (Parish)", types: ["enemies","parish"] },
    { name: "Join Warriors of Sunlight", types: ["covenant","parish"] },
    { name: "Cut off a gargoyle's tail", types: ["bosses","parish","items"] },
    { name: "Obtain Crest of Artorias", types: ["items","parish"] },
    { name: "Obtain Bloodbite Ring", types: ["rings","parish"] },
    { name: "Obtain Poisonbite Ring", types: ["rings","parish"] },
    { name: "Kill Titanite Demon (Parish)", types: ["enemies", "parish"] }
];
//darkroot forest
bingoListDKS[5] = [
    { name: "Join Forest Hunters", types: ["covenant","darkroot","npcs"] },
    { name: "Pick up Divine Ember", types: ["darkroot","ember"] },
    { name: "Pick up Enchanted Ember", types: ["darkroot","ember"] },
    { name: "Kill Black Knight (Darkroot)", types: ["darkroot","ember"] },
    { name: "Pick up Grass Crest Shield", types: ["items","armour","darkroot"] },
    { name: "Kill Sif", types: ["darkroot","bosses","sif"] },
    { name: "Pick up Hornet Ring", types: ["darkroot","bosses","sif"] },
    { name: "Pick up Wolf Ring", types: ["darkroot","items"] },
    { name: "Obtain Soul of the Moonlight Butterfly", types: ["bosses","darkroot"] }
];
//lower burg & depths
bingoListDKS[6] = [
    { name: "Rescue Griggs", types: ["npcs","lowerburg"] },
    { name: "Open Lower Burg gate to Firelink", types: ["shortcut","lowerburg"] },
    { name: "Kill Capra Demon", types: ["lowerburg","bosses"] },
    { name: "Pick up Heavy Crossbow", types: ["weapons","depths","items"] },
    { name: "Pick up Ring of the Evil Eye", types: ["rings","depths","items"] },
    { name: "Defeat Kirk (Depths)", types: ["npcs","depths","enemies"] },
    { name: "Kill Gaping Dragon", types: ["bosses","depths"] },
    { name: "Kill both Butchers", types: ["enemies","depths"] },
    { name: "Pick up Large Ember", types: ["items","ember","depths"] },
    { name: "Rescue Laurentius", types: ["npcs","depths","pyromancy"] },
    { name: "Pick up Mail Breaker", types: ["lowerburg","items","weapons"] }
];
//blighttown
bingoListDKS[7] = [
    { name: "Pick up Whip", types: ["weapons","blighttown"] },
    { name: "Pick up Server", types: ["weapons","blighttown"] },
    { name: "Pick up Iaito", types: ["weapons","blighttown"] },
    { name: "Pick up Force Within", types: ["items","spells","blighttown"] },
    { name: "Pick up Remedy", types: ["items","spells","blighttown"] },
    { name: "Pick up Poison Mist", types: ["items","spells","blighttown"] },
    { name: "Pick up Fire Keeper Soul (Blighttown)", types: ["blighttown","items","humanity"] },
    { name: "Obtain Soul of Quelaag", types: ["bosses","depths","items"] },
    { name: "Defeat Maneater Mildred", types: ["bosses","depths"] }
];
//valley of the drakes & asylum
bingoListDKS[8] = [
    { name: "Pick up Astora's Straight Sword", types: ["weapons","drakes","items"] },
    { name: "Pick up Red Tearstone Ring", types: ["drakes","items"] },
    { name: "Kill Undead Dragon (Valley of the Drakes)", types: ["bosses","drakes"] },
    { name: "Kill both Black Knights in the Asylum", types: ["asylum","enemies"] },
    { name: "Pick up Peculiar Doll", types: ["asylum","items"] },
    { name: "Pick up Rusted Iron Ring", types: ["asylum","items"] },
    { name: "Kill Stray Demon", types: ["asylum","bosses"] },
    { name: "Trade an item with Snuggly", types: ["asylum","items"] }
];
//catacombs
bingoListDKS[9] = [
    { name: "Pick up Lucerne", types: ["weapons","catacombs"] },
    { name: "Pick up Darkmoon Seance Ring", types: ["enemies","catacombs"] },
    { name: "Obtain Gravelord Greastword", types: ["covenant","catacombs"] },
    { name: "Pick up Great Scythe", types: ["catacombs"] },
    { name: "Pick up Tranquil Walk of Peace", types: ["items","spells","catacombs"] },
    { name: "Kill Black Knight (Catacombs)", types: ["items","spells","catacombs"] },
    { name: "Kill Titanite Demon (Catacombs)", types: ["enemies","catacombs"] },
    { name: "Fall for Patches' trap (Catacombs)", types: ["npcs","catacombs"] },
    { name: "Summon Paladin Leeroy", types: ["covenant","catacombs"] },
    { name: "Kill Vamos", types: ["npcs","catacombs"] }
];
//bonfires
bingoListDKS[10] = [
    { name: "Fully kindle a bonfire", types: ["bonfire"] },
    { name: "Light Sen's Fortress bonfire", types: ["sens","bonfire"] },
    { name: "Light Great Hollow bonfire", types: ["ashlake","bonfire"] },
    { name: "Light Lost Izalith bonfire", types: ["lostizalith","bonfire"] },
    { name: "Light hidden Darkroot bonfire", types: ["darkroot","bonfire"] },
    { name: "Restore Firelink Shrine bonfire", types: ["firelink","npcs","bonfire"] }
];
//demon ruins
bingoListDKS[11] = [
    { name: "Pick up Chaos Ember", types: ["demonruins","items","ember"] },
    { name: "Pick up Large Flame Ember", types: ["demonruins","items","ember"] },
    { name: "Pick up Gold-Hemmed Black Set", types: ["demonruins","bosses"] },
    { name: "Kill Ceaseless Discharge", types: ["demonruins","bosses"] },
    { name: "Murder Chaos Covenant Leader", types: ["demonruins","npcs","covenant"] },
    { name: "Activate Demon Ruins elevator", types: ["demonruins","shortcut"] },
    { name: "Join Chaos Covenant", types: ["demonruins","npcs","covenant"] },
    { name: "Get the egg head infection", types: ["demonruins","enemies"] }
];
//sen's fortress
bingoListDKS[12] = [
    { name: "Pick up Covetous Gold Serpent Ring", types: ["items","rings","sens"] },
    { name: "Pick up Ring of Steel Protection", types: ["items","rings","sens"] },
    { name: "Pick up Flame Stoneplate Ring", types: ["items","rings","sens"] },
    { name: "Pick up Sniper Crossbow", types: ["items","weapons","sens"] },
    { name: "Pick up Ricard's Rapier", types: ["items","weapons","sens","npcs"] },
    { name: "Pick up Hush", types: ["items","sens","spells"] },
    { name: "Pick up Core of an Iron Golem", types: ["sens","items","bosses"] },
    { name: "Kill Crestfallen Merchant", types: ["sens","npcs"] },
    { name: "Rescue Logan in Sen's", types: ["sens","npcs","spells"] },
    { name: "Kill all Giants in Sens", types: ["enemies","sens"] }
];
//npc interactions 1
bingoListDKS[13] = [
    { name: "Summon Solaire", types: ["covenant","solaire"] },
    { name: "Summon Iron Tarkus", types: ["npcs","lautrec"] },
    { name: "Summon Witch Beatrice", types: ["npcs","Beatrice"] },
    { name: "Kill Siegmeyer", types: ["npcs","lautrec"] },
    { name: "Kill Andre", types: ["parish","items"] },
    { name: "Kill Solaire", types: ["covenant","solaire"] }
];
//anor londo
bingoListDKS[14] = [
    { name: "Kill Lady of the Darkling", types: ["npcs","anorlondo"] },
    { name: "Kill Giant Blacksmith", types: ["anorlondo","npcs","smithy"] },
    { name: "Join Darkmoon Covenant", types: ["npcs","bosses","anorlondo"] },
    { name: "Kill Dark Sun Gwyndolin", types: ["npcs","bosses","anorlondo"] },
    { name: "Pick up Silver Knight Set", types: ["anorlondo","armour"] },
    { name: "Pick up Black Iron Set", types: ["anorlondo","guardians"] },
    { name: "Kill Smough first", types: ["anorlondo","ornsmo"] },
    { name: "Pick up Leo Ring", types: ["anorlondo","ornsmo"] },
    { name: "Invade Lautrec at Anor Londo", types: ["anorlondo","lautrec"] },
    { name: "Join Princess Guard", types: ["anorlondo","ornsmo"] },
    { name: "Kill Ornstein first", types: ["anorlondo","ornsmo"] },
    { name: "Kill Gwynevere", types: ["anorlondo","ornsmo"] },
    { name: "Obtain Tiny Being's Ring", types: ["npcs","anorlondo2"] },
    { name: "Pick up Ring of the Sun's Firstborn", types: ["anorlondo"] },
    { name: "Pick up Hawk Ring", types: ["anorlondo"] },
    { name: "Kill Titanite Demon (Anor Londo)", types: ["anorlondo","ornsmo"] },
    { name: "Pick up Havel's Set", types: ["anorlondo","armour"] }
];
//restrictions (medium)
bingoListDKS[15] = [
    { name: "Never use sorcery", types: ["restriction"] },
    { name: "Never use miracles", types: ["restriction"] },
    { name: "Never use consumables", types: ["restriction"] },
    { name: "Never use pyromancy", types: ["restriction"] },
    { name: "Never wear armour", types: ["restriction"] }
];
//new londo
bingoListDKS[16] = [
    { name: "Pick up Fire Keeper Soul (New Londo)", types: ["newlondo","items","humanity"] },
    { name: "Pick up Very Large Ember", types: ["items","newlondo",] },
    { name: "Pick up Cursebite Ring", types: ["rings","newlondo"] },
    { name: "Pick up Composite Bow", types: ["weapons","newlondo","items"] },
    { name: "Defeat Four Kings", types: ["bosses","newlondo"] },
    { name: "Kill Ingward", types: ["npcs","newlondo"] }
];
//duke's archives & crystal cave
bingoListDKS[17] = [
    { name: "Pick up Avelyn", types: ["items","weapons","dukes"] },
    { name: "Pick up Large Magic Ember", types: ["items","dukes"] },
    { name: "Die to Seath in Duke's Archives", types: ["bosses","dukes"] },
    { name: "Pick up Crystalline Set", types: ["items","crystalcave"] },
    { name: "Obtain Moonlight Greatsword", types: ["items","crystalcave","bosses"] },
    { name: "Free Sieglinde", types: ["npcs","crystalcave"] }
];
//lost izalith
bingoListDKS[18] = [
    { name: "Open Demon Ruins shortcut to Lost Izalith", types: ["lostizalith","shortcut"] },
    { name: "Defeat Kirk (Lost Izalith)", types: ["lostizalith","npcs","enemies"] },
    { name: "Pick up a Ring of Rare Sacrifice", types: ["items","lostizalith","sens"] },
    { name: "Pick up Sunlight Maggot", types: ["items","lostizalith"] }
];
//painted world
bingoListDKS[19] = [
    { name: "Pick up Dark Ember", types: ["paintedworld","ember"] },
    { name: "Pick up Red Sign Soapstone", types: ["paintedworld","guardians"] },
    { name: "Pick up Bloodshield", types: ["paintedworld","guardians"] },
    { name: "Defeat Undead Dragon (Painted World)", types: ["paintedworld","items"] },
    { name: "Defeat Jeremiah", types: ["paintedworld","enemies"] },
    { name: "Pick up Painting Guardian Set", types: ["paintedworld","armour"] },
    { name: "Pick up Velka's Rapier", types: ["paintedworld","weapons"] },
    { name: "Kill Crossbreed Priscilla", types: ["paintedworld","bosses"] }
];
//tomb of the giants
bingoListDKS[20] = [
    { name: "Kill Black Knight (Tomb of the Giants)", types: ["enemies","totg"] },
    { name: "Rescue Rhea", types: ["npcs","totg"] },
    { name: "Fall for Patches' trap (Tomb of the Giants)", types: ["npcs","totg"] },
    { name: "Pick up Skull Lantern", types: ["weapons","totg" ] },
    { name: "Pick up Effigy Shield", types: ["weapons","totg" ] },
    { name: "Pick up Large Divine Ember", types: ["ember","totg" ] },
    { name: "Pick up Covetous Silver Serpent Ring", types: ["rings","totg" ] }
];
//restrictions (hard)
bingoListDKS[21] = [
    { name: "Never light a bonfire", types: ["restriction","bonfire"] },
    { name: "Only heal with Humanity", types: ["restriction"] },
    { name: "Never upgrade weapons", types: ["restriction"] }
];
//great hollow & ash lake
bingoListDKS[22] = [
    { name: "Join Path of the Dragon", types: ["covenant","ashlake"] },
    { name: "Pick up Great Magic Barrier", types: ["miracles","ashlake"] },
    { name: "Light Ash Lake bonfire", types: ["ashlake"] },
    { name: "Pick up Cloranthy Ring", types: ["ashlake"] },
    { name: "Pick up a Titanite Slab", types: ["items"] },
    { name: "Pick up a Red Titanite Slab", types: ["items"] },
    { name: "Pick up a Blue Titanite Slab", types: ["items"] },
    { name: "Pick up a White Titanite Slab", types: ["items"] }
];
//npc interactions 2
bingoListDKS[23] = [
    { name: "Kill hollowed Crestfallen Warrior", types: ["firelink","npcs"] },
    { name: "Kill hollowed Laurentius", types: ["firelink","npcs"] },
    { name: "Kill hollowed Griggs", types: ["firelink","npcs"] },
    { name: "Kill hollowed Rhea", types: ["dukes","npcs"] },
    { name: "Battle Chaos Eaters with Siegmeyer", types: ["lostizalith","npcs"] },
    { name: "Kill crazed Solaire", types: ["lostizalith","npcs"] }
]; 
//stats
bingoListDKS[24] = [
    { name: "26 Resistance", types: ["stats","condition"] },
    { name: "36 Vitality", types: ["stats","condition"] },
    { name: "36 Endurance", types: ["stats","condition"] },
    { name: "36 Dexterity", types: ["stats","attack"] },
    { name: "36 Strength", types: ["stats","attack"] },
    { name: "28 Attunement", types: ["stats","casting"] },
    { name: "36 Intelligence", types: ["stats","casting"] },
    { name: "36 Faith", types: ["stats","casting"] },
    { name: "No Strength levelling", types: ["stats","restriction","attack"] },
    { name: "No Dexterity levelling", types: ["stats","restriction","attack"] },
    { name: "No Endurance levelling", types: ["stats","restriction","condition"] },
    { name: "No Vitality levelling", types: ["stats","restriction","condition"] }
];
//humanity & souls
bingoListDKS[25] = [
    { name: "Open the door to the Kiln", types: ["firelink","souls"] },
    { name: "Pick up 2 Soul of a Hero", types: ["items","souls"] },
    { name: "Pick up Soul of a Great Hero", types: ["items","souls"] },
    { name: "End with 20+ soft humanity", types: ["humanity"] },
    { name: "End with 200,000+ souls", types: ["souls"] }
];

//DKS2-OBJ (dark souls 2)
//starting class and item
bingoListDKS2[1] = [
    { name: "Bandit Class", types: ["startingclass","restrict_bow"] },
    { name: "Knight Class", types: ["startingclass","restrict_sword"] },
    { name: "Explorer Class", types: ["startingclass","restrict_dagger"] },
    { name: "Swordsman Class", types: ["startingclass","restrict_sword"] },
    { name: "Deprived Class", types: ["startingclass"] },
    { name: "Cleric Class", types: ["startingclass"] },
    { name: "Sorcerer Class", types: ["startingclass"] },
    { name: "Warrior Class", types: ["startingclass","restrict_sword"] },
    { name: "Seed of a Tree of Giants as Gift", types: ["startingclass"] }
];
//item restrictions
bingoListDKS2[2] = [
    { name: "No Shields", types: ["restriction","restrict_weapons"] },
    { name: "No Swords", types: ["restriction","restrict_weapons"] },
    { name: "No Polearms", types: ["restriction","restrict_weapons"] },
    { name: "Daggers Only", types: ["restriction","restrict_weapons"] },
    { name: "Axes only", types: ["restriction","restrict_weapons"] }
];
//things betwixt and majula
bingoListDKS2[3] = [
    { name: "Kill all tutorial Ogres.", types: ["startingclass"] },
    { name: "Obtain Soul Vessel (Majula Mansion)", types: ["items","majula"] },
    { name: "Buy All 3 Majula Ladders", types: ["npcs","majula"] },
    { name: "Unpetrify Rosabeth of Melfia", types: ["npcs","majula"] },
    { name: "Obtain 2 Cracked Red Eye Orbs", types: ["items","majula"] }
];
//forest of giants
bingoListDKS2[4] = [
    { name: "Kill Heide Knight (Forest of Giants)", types: ["enemies","forestofgiants"] },
    { name: "Obtain Cloranthy Ring", types: ["rings","forestofgiants"] },
    { name: "Obtain Both White Sign Soapstones", types: ["npcs","items","forestofgiants"] },
    { name: "Defeat The Last Giant", types: ["bosses","forestofgiants"] },
    { name: "Defeat The Pursuer", types: ["bosses","forestofgiants"] },
    { name: "Kill Merchant Hag Melentia", types: ["npcs","forestofgiants"] }
];
//heide and wharf
bingoListDKS2[5] = [
    { name: "Kill Blue Sentinel Targray", types: ["npcs","heide"] },
    { name: "Defeat Dragongrider (Heide)", types: ["bosses","heide"] },
    { name: "Defeat Old Dragonslayer", types: ["bosses","heide"] },
    { name: "Obtain Old Knight Halberd", types: ["items","heide"] },
    { name: "Obtain Idol's Charm", types: ["npcs","heide"] }
];
//wharf
bingoListDKS2[6] = [
    { name: "Upgrade a Pyromancy Flame to +3", types: ["pyromancy","items","wharf"] },
    { name: "Obtain Royal Soldier's Ring", types: ["bosses","wharf"] },
    { name: "Obtain Brigand Set", types: ["armour","wharf"] },
    { name: "Obtain 2x Emit Force", types: ["spells","wharf","heide"] },
    { name: "Talk to Gavlan twice", types: ["npcs","wharf","harvest"] },
    { name: "Defeat Flexile Sentry", types: ["bosses","wharf"] },
    { name: "Kill Carhillion of the Fold", types: ["npcs","heide"] }  
];
//gutter
bingoListDKS2[7] = [
    { name: "Obtain Great Club", types: ["weapons","gutter"] },
    { name: "Obtain Dark Pyromancy Flame", types: ["weapons","gutter"] },
    { name: "Obtain Bandit Greataxe", types: ["weapons","gutter"] },
    { name: "Obtain Tattered Cloth Set", types: ["armour","gutter"] },
    { name: "Obtain Havel's Set", types: ["armour","gutter","hard","forgottenkey"] },
    { name: "Defeat Melinda the Butcher", types: ["invader","gutter"] },
    { name: "Kill Giant Ant", types: ["gutter","enemies"] },
    { name: "Obtain Dark Fog", types: ["spells","straid","gutter"] }
];
//black gulch
bingoListDKS2[8] = [
    { name: "Obtain Fire Seed (Black Gulch)", types: ["fireseed","items","blackgulch","rotten"] },
    { name: "Defeat The Rotten", types: ["fireseed","items","blackgulch","rotten"] },
    { name: "Obtain Shotel", types: ["weapons","blackgulch","items"] },
    { name: "Obtain Great Magic Weapon", types: ["spells","blackgulch"] },
    { name: "Obtain Ring Of Giants +1", types: ["rings","blackgulch"] },
    { name: "Talk to Darkdiver Grandahl (Black Gulch)", types: ["darkdiver","blackgulch","npcs"] },
    { name: "Talk to Lucatiel (Black Gulch)", types: ["npcs","blackgulch","lucatiel"] },
    { name: "Obtain Forgotten Key", types: ["blackgulch","forgottenkey"] }
];
//huntsmans copse
bingoListDKS2[9] = [
    { name: "Obtain Ricard's Rapier", types: ["weapons","copse"] },
    { name: "Learn Fist Pump Gesture", types: ["npcs","copse","creighton"] },
    { name: "Obtain Soul Spear", types: ["spells","copse"] },
    { name: "Obtain Notched Whip", types: ["weapons","copse"] },
    { name: "Obtain Token of Fidelity", types: ["items","copse"] },
    { name: "Defeat Executioner's Chariot", types: ["bosses","copse"] },
    { name: "Obtain Fire Seed (Huntsman's Copse)", types: ["bosses","copse"] },
    { name: "Defeat Skeleton Lords", types: ["bosses","copse","harvest"] },
    { name: "Defeat Merciless Roenna", types: ["invader","copse","bosses"] }
];
//grave of saints
bingoListDKS2[10] = [
    { name: "Obtain Whispers of Despair", types: ["grave","spells"] },
    { name: "Obtain Great Heal", types: ["grave","spells"] },
    { name: "Defeat Rhoy the Explorer", types: ["grave","invader"] },
    { name: "Obtain Disc Chime", types: ["grave","items"] },
    { name: "Defeat Royal Rat Vanguard", types: ["grave","bosses"] },
    { name: "Obtain Token of Spite", types: ["grave","items"] }
];
//lost bastille & sinners 1
bingoListDKS2[11] = [
    { name: "Obtain Large Club", types: ["bastille","items"] },
    { name: "Obtain Twinblade", types: ["bastille","weapons"] },
    { name: "Obtain Bracing Knuckle Ring", types: ["bastille","rings"] },
    { name: "Defeat Ruin Sentinels", types: ["bastille","bosses"] },
    { name: "Obtain Wanderer Set", types: ["bastille","armour"] },
    { name: "Obtain Soul Vessel (Lost Bastille)", types: ["bastille","weapons"] }
];
//lost bastille & sinners 2
bingoListDKS2[12] = [
    { name: "Obtain Hush", types: ["spells","bastille"] },
    { name: "Obtain Covetous Silver Serpent Ring", types: ["items","rings","bastille"] },
    { name: "Unpetrify Straid of Olaphis", types: ["branch","npcs","bastille"] },
    { name: "Obtain Northern Ritual Band", types: ["items","rings","bastille"] },
    { name: "Light both sides of Lost Sinner's chamber", types: ["items","belfrykey","bastille"] },
    { name: "Obtain Fire Seed (Sinner's Rise)", types: ["items","belfrykey","bastille"] }
];
//harvest valley
bingoListDKS2[13] = [
    { name: "Obtain Both Fragrant Branches in Harvest Valley", types: ["harvest","items"] },
    { name: "Obtain Old Knight Pike", types: ["harvest","hard","enemies"] },
    { name: "Obtain 'Praise The Sun' Gesture", types: ["gesture","harvest"] },
    { name: "Defeat Covetous Demon", types: ["parish","items"] },
    { name: "Obtain Poisonbite Ring", types: ["covenant","solaire"] }
];
//earthen peak
bingoListDKS2[14] = [
    { name: "Obtain Heavy Crossbow +3", types: ["weapons","epeak","upgrade"] },
    { name: "Obtain Mirrah Shield", types: ["weapons","epeak"] },
    { name: "Obtain Spell Quartz Ring +1", types: ["rings","epeak"] },
    { name: "Obtain Petrified Something (Earthen Peak)", types: ["bosses","epeak"] },
    { name: "Defeat Mytha, The Baneful Queen", types: ["epeak","bosses"] },
    { name: "Summon Jester Thomas", types: ["epeak","bosses"] },
    { name: "Purchase Gilligan's Ladder (Earthen Peak)", types: ["epeak","npcs"] }
];
//iron keep & belfry sol 1
bingoListDKS2[15] = [
    { name: "Obtain Dull Ember", types: ["keep","items"] },
    { name: "Obtain Covetous Gold Serpent Ring", types: ["keep","rings"] },
    { name: "Obtain Zweihander", types: ["weapons","keep"] },
    { name: "Defeat Smelter Demon", types: ["keep","bosses"] },
    { name: "Summon Lucatiel (Iron Keep)", types: ["keep","npc"] },
    { name: "Obtain Life Ring +1", types: ["keep","rings"] }
];
//iron keep & belfry sol 2
bingoListDKS2[16] = [
    { name: "Ring Belfry Sol Bell", types: ["bells","keep"] },
    { name: "Obtain Dark Armor (Chest)", types: ["bells","armour"] },
    { name: "Obtain Black Knight Great Axe", types: ["bells","keep"] },
    { name: "Defeat Old Iron King", types: ["keep","bosses"] },
    { name: "Obtain Phoenix Parma Shield", types: ["keep","items","hard"] },
    { name: "Obtain Iron Key and Open Associated Door", types: ["keep","forestofgiants","key"] }
];
//more restrictions
bingoListDKS2[17] = [
    { name: "Never use Sorcery", types: ["restriction","restrict_spell"] },
    { name: "Never use Miracles", types: ["restriction","restrict_spell"] },
    { name: "Never use Hexes", types: ["restriction","restrict_spell"] },
    { name: "Never use Consumables", types: ["restriction", "restrict_consumable"] },
    { name: "Never Heal with Estus", types: ["restriction", "restrict_heal"] },
    { name: "Never use Pyromancy", types: ["restriction","restrict_spell"] },
    { name: "Never wear Armour", types: ["restriction"] },
    { name: "Only equip Weapons in your Left Hand", types: ["restriction","restrict_weapons"] }
];
//shaded woods
bingoListDKS2[18] = [
    { name: "Obtain Chloranthy Ring +1", types: ["shaded","rings"] },
    { name: "Obtain Old Sun Ring", types: ["shaded","rings"] },
    { name: "Obtain Vengarl Helm", types: ["npcs","shaded"] },
    { name: "Summon Manscorpion Tark", types: ["npcs","shaded"] },
    { name: "Free Ornifex", types: ["branch","npcs","shaded"] },
    { name: "Talk to Darkdiver Grandahl (Shaded Woods)", types: ["shaded","darkdiver","npcs"] },
    { name: "Obtain Gold Falcon Shield", types: ["weapons","shaded"] },
    { name: "Defeat Scorpioness Najka", types: ["bosses","shaded"] },
    { name: "Kill Giant Basilisk (Shaded Woods)", types: ["enemies","shaded"] },
    { name: "Obtain Red Tearstone Ring", types: ["rings","shaded"] }
];
//doors of pharros
bingoListDKS2[19] = [
    { name: "Obtain Dark Scorpion Stinger", types: ["doors","weapons"] },
    { name: "Obtain Santier's Spear", types: ["doors","weapons"] },
    { name: "Obtain Twisted Barricade", types: ["doors","items"] },
    { name: "Defeat Bowman Guthry", types: ["doors","invader","bosses"] },
    { name: "Defeat Royal Rat Authority", types: ["doors","bosses"] }
];
//brightstone cove
bingoListDKS2[20] = [
    { name: "Obtain Southern Ritual Band +1", types: ["brightstone","rings"] },
    { name: "Obtain Crescent Axe", types: ["brightstone","weapons"] },
    { name: "Defeat Prowling Magus and Congregation", types: ["brightstone","bosses"] },
    { name: "Obtain Soul Vortex", types: ["brightstone","spells"] },
    { name: "Obtain Lacerating Arrows", types: ["brightstone","items"] }
];
//even more restrictions
bingoListDKS2[21] = [
    { name: "No Rings", types: ["restriction","restrict_armour"] },
    { name: "Starting Armour only", types: ["restriction","restrict_armour"] },
    { name: "Starting Weapons only", types: ["restriction","restrict_armour"] },
    { name: "No Torch usage", types: ["restriction","restrict_armour"] },
    { name: "Only Heal with Estus", types: ["restriction","restrict_heal"] }
];
//belfry luna & things
bingoListDKS2[22] = [
    { name: "Defeat Belfry Gargoyles", types: ["bastille","belfry","bosses"] },
    { name: "Ring Belfry Luna Bell", types: ["bastille","belfry","bosses"] },
    { name: "Summon Lucatiel (Lost Bastille)", types: ["bastille","npcs"] },
    { name: "Never Light a Bonfire", types: ["restriction"] },
    { name: "Get +5 Estus Flask", types: ["restriction"] },
    { name: "Never Upgrade Weapons", types: ["restriction","hard"] }
];
//majula things
bingoListDKS2[23] = [
    { name: "Join and Remain in the Company of Champions (Before any Other Goals)", types: ["covenant","majula"] },
    { name: "Purchase a piece of the Alva Set", types: ["maughlin","majula"] },
    { name: "Obtain Invisible Aurous Set", types: ["maughlin","majula"] },
    { name: "Obtain 5 Estus Charges", types: ["emerald","majula"] },
    { name: "Obtain Ring of Steel Protection", types: ["rings","majula"] }
]; 
//humanity souls and stats
bingoListDKS2[24] = [
    { name: "150000 souls", types: ["stats","souls"] },
    { name: "40 Adaptability", types: ["vessel","stats","condition"] },
    { name: "40 Vitality", types: ["vessel","stats","condition","vit"] },
    { name: "40 Endurance", types: ["vessel","stats","condition","end"] },
    { name: "No Endurance levelling", types: ["vessel","stats","restriction","condition","vit"] },
    { name: "No Vigor levelling", types: ["vessel","stats","restriction","condition"] },
    { name: "No Vitality levelling", types: ["vessel","stats","restriction","condition","end"] }
];
//humanity souls and stats
bingoListDKS2[25] = [
    { name: "36 Dexterity", types: ["vessel","stats","attack"] },
    { name: "36 Strength", types: ["vessel","stats","attack"] },
    { name: "28 Attunement", types: ["vessel","stats","casting"] },
    { name: "36 Intelligence", types: ["vessel","stats","casting"] },
    { name: "36 Faith", types: ["vessel","stats","casting"] }
];

//DKS3-OBJ (dark souls 3)
//starting class and item
bingoListDKS3[1] = [
    { name: "Knight Class", types: ["startingclass"] },
    { name: "Mercenary Class", types: ["startingclass"] },
    { name: "Warrior Class", types: ["startingclass"] },
    { name: "Herald Class", types: ["startingclass"] },
    { name: "Thief Class", types: ["startingclass"] },
    { name: "Assassin Class", types: ["startingclass"] },
    { name: "Sorceror Class", types: ["startingclass"] },
    { name: "Pyromancer Class", types: ["startingclass"] },
    { name: "Cleric Class", types: ["startingclass"] },
    { name: "Deprived Class", types: ["startingclass"] }
];
//restrictions simple
bingoListDKS3[2] = [
    { name: "No Shields", types: ["restriction","restrict_weapons"] },
    { name: "Starting Equipment Only", types: ["restriction","restrict_weapons","starting_equpiment","restrict_armour"] },
    { name: "No Armour", types: ["restriction","restrict_armour","free"] },
    { name: "Do not use Pyromancy", types: ["restriction","restrict_spells","free"] },
    { name: "Do not use Miracles", types: ["restriction","restrict_spells","free"] },
    { name: "No Ashen Estus", types: ["restriction","restrict_recovery"] },
    { name: "No Ranged Weapons", types: ["restriction","restrict_weapons"] }
];
//high wall
bingoListDKS3[3] = [
    { name: "Obtain Refined Gem", types: ["highwall","kill"] },
    { name: "Obtain Astora's Straight Sword", types: ["highwall"] },
    { name: "Defeat Dancer of the Boreal Valley", types: ["highwall","item"] },
    { name: "Obtain Claymore", types: ["highwall","item"] },
    { name: "Kill High Priestess Emma", types: ["highwall","npckill"] },
    { name: "Kill Greirat", types: ["npckill","greirat"] }
];
//firelink
bingoListDKS3[4] = [
    { name: "Obtain Fire Keeper's Soul", types: ["firelink","item"] },
    { name: "Obtain Uchigatana", types: ["firelink","item"] },
    { name: "Obtain Armour of the Sun (Chestpiece)", types: ["firelink","crowtrade","siegward"] },
    { name: "Obtain Porcine Shield", types: ["firelink","crowtrade","item"] },
    { name: "Obtain 'Hello' Carving", types: ["firelink","crowtrade","item"] },
    { name: "Obtain Lucatiel Mask", types: ["firelink","crowtrade","item"] },
    { name: "Kill Andre of Astora", types: ["firelink","npckill"] }
];
//undead settlement
bingoListDKS3[5] = [
    { name: "Obtain Caduceus Round Shield", types: ["undeadsettlement","item"] },
    { name: "Obtain Mound Maker's Covenant", types: ["undeadsettlement","item"] },
    { name: "Defeat Curse Rotted Greatwood", types: ["undeadsettlement","bosskill","item"] },
    { name: "Befriend the Giant", types: ["undeadsettlement","free"] },
    { name: "Obtain Mortician's Ashes", types: ["undeadsettlement","item"] },
    { name: "Defeat Boreal Knight (Before Road of Sacrifices)", types: ["undeadsettlement"] },
    { name: "Obtain Chloranthy Ring", types: ["undeadsettlement","item"] },
    { name: "Kill Siegward of Catarina", types: ["firelink","npckill","siegward"] }
];
//road of sacrifices before swamp
bingoListDKS3[6] = [
    { name: "Obtain Butcher's Knife", types: ["roadofsac","item"] },
    { name: "Obtain Morne's Ring", types: ["roadofsac","item"] },
    { name: "Obtain Twin Dragon Greatshield", types: ["roadofsac","item"] },
    { name: "Obtain Ring of Avarice", types: ["roadofsac","item"] },
    { name: "Obtain Grass Crest Shield", types: ["roadofsac","item"] },
    { name: "Obtain Fallen Knight Set", types: ["roadofsac","item"] },
    { name: "Obtain Farron Coal", types: ["roadofsac","item"] },
    { name: "Obtain Exile Greatsword", types: ["roadofsac","item"] },
    { name: "Obtain Great Club", types: ["roadofsac","item"] },
    { name: "Defeat Crystal Sage (Road of Sacrifices)", types: ["roadofsac","item"] },
    { name: "Kill Anri of Astora", types: ["npckill","anri"] }
];
//farron keep
bingoListDKS3[7] = [
    { name: "Obtain Black Bow of Pharis", types: ["farronkeep","item"] },
    { name: "Defeat Stray Demon", types: ["farronkeep","bosskill"] },
    { name: "Defeat Abyss Watchers", types: ["farronkeep","bosskill"] },
    { name: "Obtain Antiquated Set", types: ["farronkeep","item"] },
    { name: "Obtain Sunlight Talisman", types: ["farronkeep","free","item"] },
    { name: "Obtain Lingering Dragoncrest Ring", types: ["farronkeep","item"] }
];
//cathedral of the deep
bingoListDKS3[8] = [
    { name: "Obtain Drang Set", types: ["deacons","item"] },
    { name: "Obtain Saint Bident", types: ["deacons","item"] },
    { name: "Obtain Notched Whip", types: ["deacons","item"] },
    { name: "Obtain Red Sign Soapstone", types: ["deacons","finger","item"] },
    { name: "Obtain Rosaria's Fingers Covenant", types: ["deacons","finger"] },
    { name: "Obtain Aldrich's Sapphire", types: ["deacons","kill"] },
    { name: "Trigger Patches cutscene at Cathedral of the Deep", types: ["deacons","patches"] },
    { name: "Defeat Deacons of the Deep", types: ["deacons","bosskill"] },
    { name: "Kill Unbreakable Patches", types: ["firelink","npckill"] }
];
//catacombs
bingoListDKS3[9] = [
    { name: "Obtain Black Blade", types: ["catacombs","item"] },
    { name: "Obtain Carthus Pyromancy Tome", types: ["catacombs","item"] },
    { name: "Obtain Carthus Milkring", types: ["catacombs","item"] },
    { name: "Obtain Carthus Bloodring", types: ["catacombs","item"] },
    { name: "Obtain Grave Warden's Ashes", types: ["catacombs","item"] },
    { name: "Obtain Soul of a Demon (Catacombs)", types: ["catacombs","kill"] },
    { name: "Obtain Grave Warden Pyromancy Tome", types: ["catacombs","bosskill"] },
    { name: "Defeat High Lord Wolnir", types: ["catacombs","bosskill"] },
    { name: "Kill Hoarce the Hushed", types: ["npckill","horace"] }
];
//irithyll valley
bingoListDKS3[10] = [
    { name: "Obtain Pontiff's Right Eye", types: ["irithyll1","kill"] },
    { name: "Obtain Smough’s Great Hammer", types: ["irithyll1","item"] },
    { name: "Obtain Dorhys' Gnawing", types: ["irithyll1","kill"] },
    { name: "Obtain Easterner's Ashes", types: ["irithyll1","item"] },
    { name: "Obtain Excrement-covered Ashes", types: ["irithyll1","item"] },
    { name: "Defeat Pontiff Sulyvahn", types: ["irithyll1","bosskill"] }  
];
//irithyll dungeon
bingoListDKS3[11] = [
    { name: "Obtain Lightning Blade", types: ["irithyll2","item"] },
    { name: "Obtain Dark Clutch Ring", types: ["irithyll2","item"] },
    { name: "Obtain Dragon Torso Stone", types: ["irithyll2","item"] },
    { name: "Obtain Xanthous Ashes", types: ["irithyll2","item"] },
    { name: "Obtain Pickaxe", types: ["irithyll2","item"] }
];
//anor londo 
bingoListDKS3[12] = [
    { name: "Defeat Aldrich", types: ["anorlondo","bosskill"] },
    { name: "Obtain Aldrich Faithful Covenant", types: ["anorlondo","item"] },
    { name: "Obtain Dragonslayer Greatbow", types: ["anorlondo","item"] },
    { name: "Obtain Reversal Ring", types: ["anorlondo","item"] },
    { name: "Obtain Painting Guardian Set", types: ["anorlondo","item"] }
];
//profaned capital
bingoListDKS3[13] = [
    { name: "Defeat Yhorm the Giant", types: ["profaned","bosskill"] },
    { name: "Defeat Yhorm the Giant without Storm Ruler or Siegward", types: ["profaned","bosskill","restriction"] },
    { name: "Obtain Eleonora", types: ["profaned","item"] },
    { name: "Obtain Court Sorcerer's Staff", types: ["profaned","item"] }
];
//lothric castle 1
bingoListDKS3[14] = [
    { name: "Defeat Dragonslayer Armour", types: ["lothric","bosskill"] },
    { name: "Defeat Boreal Knight (Lothric Castle)", types: ["lothric"] },
    { name: "Defeat Both Dragons (Lothric Castle)", types: ["lothric"] },
    { name: "Obtain Winged Knight Set", types: ["lothric","item"] },
    { name: "Obtain Red Tearstone Ring", types: ["lothric","item"] }
];
//consumed kings gardens
bingoListDKS3[15] = [
    { name: "Defeat Oceiros the Consumed King", types: ["consumed","bosskill"] },
    { name: "Obtain Shadow Set", types: ["consumed","item"] },
    { name: "Obtain Dragonscale Ring", types: ["consumed","item"] },
    { name: "Obtain Magic Stoneplate Ring", types: ["consumed","item"] }
];
//smouldering lake 1
bingoListDKS3[16] = [
    { name: "Obtain Black Knight Sword", types: ["smouldering","item"] },
    { name: "Obtain Izalith Pyromancy Tome", types: ["smouldering","item"] },
    { name: "Obtain White Hair Talisman", types: ["smouldering","item"] },
    { name: "Defeat Carthus Sandworm Miniboss", types: ["smouldering"] }
];
bingoListDKS3[17] = [
    { name: "Defeat Old Demon King", types: ["smouldering","bosskill"] },
    { name: "Obtain Dragonrider Bow", types: ["smouldering","avelyn","item"] },
    { name: "Defeat Knight Slayer Tsorig (Human)", types: ["smouldering","tsorig"] },
    { name: "Deactivate Giant Avelyn", types: ["smouldering","avelyn"] }
];
//grand archives
bingoListDKS3[18] = [
    { name: "Defeat Lothric, Younger Prince", types: ["grandarchives","bosskill"] },
    { name: "Obtain Avelyn", types: ["grandarchives","item"] },
    { name: "Obtain Witch's Locks", types: ["grandarchives","item"] },
    { name: "Defeat Human Trio in Grand Archives", types: ["grandarchives","kill"] },
    { name: "Defeat Boreal Knight (Grand Archives)", types: ["grandarchives","item"] },
    { name: "Obtain Crystal Scroll", types: ["grandarchives","item"] }
];
//npc recovery
bingoListDKS3[19] = [
    { name: "Have Cornyx at Firelink", types: ["npcrecovery","npcearly"] },
    { name: "Free Greirat", types: ["npcrecover","npcearly"] },
    { name: "Have Orbeck of Vinheim at Firelink", types: ["npcrecovery","npcmid"] },
    { name: "Have Yuria of Londor at Firelink", types: ["npcrecovery","npcmid"] },
    { name: "Have Irina at Firelink", types: ["npcrecovery","npcmid"] }
];
//restrictions harder
bingoListDKS3[20] = [
    { name: "Do not change equipment", types: ["restriction","restrict_armour","starting_equpiment"] },
    { name: "Do not use Sorceries", types: ["restriction","restrict_spells"] },  
    { name: "Do not use Swords", types: ["restriction","restrict_weapons"] },
    { name: "Do not use Estus", types: ["restriction","restrict_recovery"] },
    { name: "Do not equip Rings", types: ["restriction","restrict_recovery"] },
    { name: "Do not level at a Fire Keeper", types: ["restriction","restrict_level"] }
];
//npc invaders
bingoListDKS3[21] = [
    { name: "Obtain Armour of Thorns", types: ["invaders","deacons","kirk","item"] },
    { name: "Defeat Holy Knight Hodrick (Invasion)", types: ["invaders","undeadsettlement"] },
    { name: "Defeat Daughter of Crystal Kriemhild (Invasion)", types: ["invaders","untended"] },
    { name: "Defeat Yellowfinger Heysel (Invasion)", types: ["invaders"] },
    { name: "Defeat Knight Slayer Tsorig (Invasion)", types: ["invaders","tsorig"] }
];
//untended graves
bingoListDKS3[22] = [
    { name: "Purchase Wolf Knight Helm", types: ["untended","gundyr","item"] },
    { name: "Obtain Blacksmith hammer", types: ["untended","gundyr","item"] },
    { name: "Defeat Champion Gundyr", types: ["untended","gundyr"] },
    { name: "Obtain Black Knight Glaive", types: ["untended","gundyr"] },
    { name: "Obtain Chaos Blade", types: ["untended","item"] },
    { name: "Obtain Ashen Estus Ring", types: ["untended","item"] },
    { name: "Give Fire Keeper the Eyes of a Fire Keeper", types: ["untended","gundyr"] }
];
//archdragon peak
bingoListDKS3[23] = [
    { name: "Defeat the Nameless King", types: ["bosskill","archdragon"] },
    { name: "Defeat Ancient Wyvern without Plunging Attacks", types: ["bosskill","archdragon"] },
    { name: "Obtain Twinkling Dragon Head Stone", types: ["bosskill","archdragon","npclong"] },
    { name: "Obtain Havel's Greatshield", types: ["archdragon","kill"] },
    { name: "Obtain Dragon Chaser's Ashes", types: ["archdragon","item"] },
    { name: "Obtain Calamity Ring", types: ["archdragon","item"] },
    { name: "Obtain Dragonslayer Spear", types: ["archdragon","item"] }
];
//npc longer term
bingoListDKS3[24] = [
    { name: "Have Karla at Firelink", types: ["npcrecovery","npclong"] },
    { name: "Invade and kill Leonhard at Anor Londo", types: ["npclong","finger"] },
    { name: "Free Siegward in Irithyll Dungeon", types: ["npclong","siegward"] },
    { name: "Help Anri defeat Aldrich", types: ["npclong","anri"] },
    { name: "Marry Anri", types: ["npclong","anri"] },
    { name: "Help Sirris defeat Holy Knight Hodrick", types: ["npclong"] }
];
//more involved things
bingoListDKS3[25] = [
    { name: "Obtain Darkmoon Covenant", types: ["involved"] },
    { name: "Completely upgrade a Weapon", types: ["involved"] },
    { name: "Completely upgrade a Shield", types: ["involved"] },
    { name: "Completely upgrade Pyromancy Flame", types: ["involved"] },
    { name: "Completely upgrade a Talisman", types: ["involved"] },
    { name: "Completely upgrade a Catalyst", types: ["involved"] },
    { name: "10 or more Estus Flask Charges", types: ["involved"] },
    { name: "Estus Flask +7 or Higher ", types: ["involved"] },
    { name: "Obtain 2 Titanite Slabs", types: ["involved"] }
];

//DMS-OBJ (demon's souls)
//starting class and item
bingoListDMS[1] = [
    { name: "Soldier Class", types: ["startingclass"] },
    { name: "Knight Class", types: ["startingclass"] },
    { name: "Hunter Class", types: ["startingclass"] },
    { name: "Priest Class", types: ["startingclass"] },
    { name: "Magician Class", types: ["startingclass"] },
    { name: "Wanderer Class", types: ["startingclass"] },
    { name: "Barbarian Class", types: ["startingclass"] },
    { name: "Thief Class", types: ["startingclass"] },
    { name: "Temple Knight Class", types: ["startingclass"] },
    { name: "Royalty Class", types: ["startingclass"] }
];
//restrictions
bingoListDMS[2] = [
    { name: "No Shields", types: ["restriction","restrict_weapons"] },
    { name: "No Melee Weapons", types: ["restriction","restrict_weapons"] },
    { name: "Starting Weapons Only", types: ["restriction","restrict_weapons"] }
];
//boletarian palace 1
bingoListDMS[3] = [
    { name: "Obtain Cling Ring.", types: ["world1"] },
    { name: "Obtain Brass Telescope", types: ["world1"] },
    { name: "Obtain Ring of Great Strength", types: ["world1"] },
    { name: "Obtain Ring of Herculean Strength", types: ["world1"] }
];
//boletarian palace 2
bingoListDMS[4] = [
    { name: "Obtain Large Brushwood Shield", types: ["world1"] },
    { name: "Obtain Tower Shield", types: ["world1","npcmurder"] },
    { name: "Obtain Spiked Shield", types: ["world1","npcmurder"] }
];
//stonefang tunnel
bingoListDMS[5] = [
    { name: "Obtain Kris Blade", types: ["world2"] },
    { name: "Obtain Crushing Battle Axe +1", types: ["world2"] },
    { name: "Obtain Great Club", types: ["world2"] }
];
//tower of latria
bingoListDMS[6] = [
    { name: "Obtain Mercury Rapier +1", types: ["world3"] },
    { name: "Obtain Ring of Magical Sharpness", types: ["world3"] },
    { name: "Obtain Moon Short Sword +1", types: ["world3"] },
    { name: "Obtain Ring of Avarice", types: ["world3"] }
];
//shrine of storms 1
bingoListDMS[7] = [
    { name: "Obtain Crescent Falchion +1", types: ["world4"] },
    { name: "Obtain Hiltless", types: ["world4"] },
    { name: "Trade an item with Sparkly the Crow", types: ["world4"] }
];
//valley of defilement 1
bingoListDMS[8] = [
    { name: "Obtain Ring of Magical Dullness", types: ["world5"] },
    { name: "Obtain Blessed Mace +1", types: ["world5"] },
    { name: "Obtain Morning Star", types: ["world5"] }
];
//stonefang tunnel 2
bingoListDMS[9] = [
    { name: "Obtain Dragon Long Sword +1", types: ["world2"] },
    { name: "Obtain Dark Heater Shield +2", types: ["world2"] },
    { name: "Obtain Master's Ring", types: ["world2"] }
];
//boss kills
bingoListDMS[10] = [
    { name: "Kill Dragon God", types: ["bosskill","world2boss"] },
    { name: "Kill Old Monk", types: ["bosskill","world3boss"] },
    { name: "Kill Storm Ruler", types: ["bosskill","world4boss"] }
];
//npc kills
bingoListDMS[11] = [
    { name: "Kill Blacksmith Boldwin", types: ["npcmurder"] },
    { name: "Kill Blacksmith Ed", types: ["npcmurder"] },
    { name: "Kill Patches the Hyena", types: ["npcmurder"] }
];
bingoListDMS[12] = [
    { name: "Kill Stockpile Tomhas", types: ["npcmurder"] },
    { name: "Kill Mephistopheles", types: ["npcmurder"] },
    { name: "Kill the Dregling Merchant", types: ["npcmurder"] }
];
bingoListDMS[13] = [
    { name: "Kill the Filthy Man", types: ["npcmurder"] },
    { name: "Kill the Once Royal Mistress", types: ["npcmurder"] },
    { name: "Kill Graverobber Blige", types: ["npcmurder"] }
];
bingoListDMS[14] = [
    { name: "Kill the Filthy Woman", types: ["npcmurder"] },
    { name: "Kill Vanguard", types: ["npcmurder"] },
    { name: "Kill the Once Royal Mistress", types: ["npcmurder"] }
];
//misc
bingoListDMS[15] = [
    { name: "Fully Upgrade a Weapon", types: ["upgrade"] },
    { name: "Fully Upgrade a Shield", types: ["upgrade"] }
];
//restrictions 2
bingoListDMS[16] = [
    { name: "No use of Miracles", types: ["restriction","restrict_weapons"] },
    { name: "No use of Magic", types: ["restriction","restrict_weapons"] },
    { name: "No Cling Ring", types: ["restriction","restrict_weapons"] }
];
//boss kills 2
bingoListDMS[17] = [
    { name: "Kill False King Allant", types: ["bosskill","world1boss"] },
    { name: "Kill Maiden Astraea", types: ["bosskill","world5boss"] },
    { name: "Kill the Penetrator with Biorr", types: ["bosskill","world1boss","biorr"] }
];
//valley of defilement 2
bingoListDMS[18] = [
    { name: "Obtain Blind", types: ["world5"] },
    { name: "Obtain Mirdan Hammer", types: ["world5"] },
    { name: "Obtain Dark Silver Shield", types: ["world5"] }
];
//shrine of storms 2
bingoListDMS[19] = [
    { name: "Obtain Ring of Devout Prayer", types: ["world4"] },
    { name: "Obtain Ronin's Ring", types: ["world4"] },
    { name: "Obtain Storm Ruler", types: ["world4"] }
];
//boletarian palace 3
bingoListDMS[20] = [
    { name: "Obtain Official's Hat", types: ["world1","world1bosskill"] },
    { name: "Obtain Demonbrandt", types: ["world1","npcmurder"] },
    { name: "Obtain Purple Flame Shield", types: ["world1","npcmurder"] }
];
//restrictions 3
bingoListDMS[21] = [
    { name: "No Levelling", types: ["restriction","restrict_weapons"] },
    { name: "Never Unequip Items", types: ["restriction","restrict_weapons"] },
    { name: "No Ranged Weapons", types: ["restriction","restrict_weapons"] }
];
//filler
bingoListDMS[22] = [
    { name: "Obtain White Bow", types: ["world4"] },
    { name: "Obtain Penetrating Sword", types: ["world1","world1bosskill"] },
    { name: "Obtain Silver Bracelet", types: ["world3"] },
    { name: "Obtain Large Sword of Moonlight", types: ["world5"] }
];
//world tendency 1
bingoListDMS[23] = [
    { name: "Obtain Magic Sword Makoto", types: ["worldtendency"] },
    { name: "Obtain Istarelle", types: ["worldtendency"] },
    { name: "Obtain Phosphorescent Pole", types: ["worldtendency"] }
];
//world tendency 2
bingoListDMS[24] = [
    { name: "Obtain Colorless Demon's Soul", types: ["worldtendency"] },
    { name: "Obtain Guillotine Axe", types: ["worldtendency"] },
    { name: "Obtain Dragon Bone Smasher", types: ["worldtendency"] }
];
//world tendency 3
bingoListDMS[25] = [
    { name: "Obtain Bramd", types: ["worldtendency"] },
    { name: "Obtain Talisman of Beasts", types: ["worldtendency"] },
    { name: "Obtain Dull Gold Helmet", types: ["worldtendency"] }
];

//MP-OBJ (mendon ponds)
//major ponds
bingoListMP[1] = [
    { name: "Run around Hundred Acre Pond", types: ["pond", "achievement"] },
    { name: "Run around Deep Pond", types: ["pond", "achievement"] },
    { name: "Run around Quaker Pond", types: ["pond", "achievement"] }
];
//lodges
bingoListMP[2] = [
    { name: "Visit Stewart Lodge", types: ["lodge", "landmark"] },
    { name: "Visit Hopkins Point Lodge", types: ["lodge", "landmark"] },
    { name: "Visit Cavalry Lodge", types: ["lodge", "landmark"] },
    { name: "Visit West and East Lodges", types: ["lodge", "landmark"] }
];
//major shelters
bingoListMP[3] = [
    { name: "Visit Lookout Shelter", types: ["shelter", "landmark"] },
    { name: "Visit Canfield Woods Shelter", types: ["shelter", "landmark"] },
    { name: "Visit Pond View Shelter", types: ["shelter", "landmark"] },
    { name: "Visit Algonkian Shelter", types: ["shelter", "landmark"] }
];
//easy trail completes
bingoListMP[4] = [
    { name: "Climb the stairs in Devil's Bathtub", types: ["trail", "achievement"] },
    { name: "Run the boardwalk on Quaker Pond", types: ["trail", "achievement"] },
    { name: "Climb Cardiac Hill", types: ["trail", "achievement"] }
];
//spigots
bingoListMP[5] = [
    { name: "Drink from the beach spigot", types: ["spigot", "landmark"] },
    { name: "Drink from the Lookout spigot", types: ["spigot", "landmark"] },
    { name: "Drink from the Cavalry spigot", types: ["spigot", "landmark"] },
    { name: "Drink from the Hopkins Point spigot", types: ["spigot", "landmark"] },
    { name: "Drink from the Southview spigot", types: ["spigot", "landmark"] }
];
//other major landmarks
bingoListMP[6] = [
    { name: "Swing on Stewart swingset", types: ["major", "landmark"] },
    { name: "Swing on Hopkins Point swingset", types: ["major", "landmark"] },
    { name: "Swing on Cavalry swingset", types: ["major", "landmark"] },
    { name: "Circle eastern water tower", types: ["water_tower", "major", "landmark"] },
    { name: "Visit Cobblestone House", types: ["major", "landmark"] },
    { name: "Visit Hundred Acre Pond boat launch", types: ["major", "landmark"] }
];
//easy trail restrictions
bingoListMP[7] = [
    { name: "Don't run on any Pine Woods trails", types: ["pinewoods", "trail", "restriction"] },
    { name: "Don't run on any Grasslands trails", types: ["grasslands", "trail", "restriction"] },
    { name: "Don't run on any Fern Valley trails", types: ["fernvalley", "trail", "restriction"] },
    { name: "Don't run on any North Meadow trails", types: ["northmeadow", "trail", "restriction"] }
];
//easy category combos
bingoListMP[8] = [
    { name: "Visit all four lodges", types: ["lodge", "landmark"] },
    { name: "Swing on two swingsets", types: ["major", "landmark"] },
    { name: "Drink from three spigots", types: ["spigot", "landmark"] },
    { name: "Visit two shelters", types: ["shelter", "landmark"] },
    { name: "Climb Cardiac Hill three different ways", types: ["trail", "achievement"] }
];
//trail scavenger hunt I
bingoListMP[9] = [
    { name: "Pick up some trail trash", types: ["hunt"] },
    { name: "Hop over a tree fallen across the trail", types: ["hunt"] },
    { name: "Sit on a bench carved out of a giant tree trunk", types: ["hunt"] },
    { name: "Run down your favorite sledding hill", types: ["hunt"] }
];
//minor ponds
bingoListMP[10] = [
    { name: "Run around Devil's Bathtub", types: ["pond", "achievement"] },
    { name: "Run around Round Pond", types: ["pond", "achievement"] }
];
//sign visits
bingoListMP[11] = [
    { name: "Visit west Canfield entrance sign", types: ["sign", "landmark"] },
    { name: "Visit east Canfield entrance sign", types: ["sign", "landmark"] },
    { name: "Visit Hopkins Point entrance sign", types: ["sign", "landmark"] },
    { name: "Visit the nature center entrance sign", types: ["sign", "landmark"] }
];
//minor shelters
bingoListMP[12] = [
    { name: "Visit Evergreen Shelter", types: ["shelter", "landmark"] },
    { name: "Visit Devil's Bathtub Shelter", types: ["shelter", "landmark"] },
    { name: "Visit Southview Shelter", types: ["shelter", "landmark"] }
];
//moderate trail completes
bingoListMP[13] = [
    { name: "Run David's Nature Trail", types: ["trail", "achievement"] },
    { name: "Run the length of the west esker", types: ["trail", "achievement"] },
    { name: "Run to Mendon Center Road trailhead", types: ["trail", "achievement"] },
    { name: "Circle the end of South Wilmarth Road", types: ["road", "achievement"] }
];
//flora and fauna
bingoListMP[14] = [
    { name: "Spot a deer", types: ["wildlife", "achievement"] },
    { name: "Forage some wild fruit", types: ["wildlife", "achievement"] },
    { name: "Feed a chickadee", types: ["wildlife", "achievement"] },
    { name: "Spot a black squirrel", types: ["wildlife", "achievement"] }
];
//other minor landmarks
bingoListMP[15] = [
    { name: "Circle the northern water tower", types: ["water_tower", "minor", "landmark"] },
    { name: "Run through the rugby goalposts", types: ["minor", "landmark"] },
    { name: "Run around a flagpole", types: ["minor", "landmark"] },
    { name: "Visit a ski sign rated easy", types: ["minor", "landmark"] },
    { name: "Visit a ski sign rated more difficult", types: ["minor", "landmark"] },
    { name: "Visit a ski sign rated most difficult", types: ["minor", "landmark"] }
];
//trail scavenger hunt 2
bingoListMP[16] = [
    { name: "Find a 'no horses' sign", types: ["hunt"] },
    { name: "Find a sign depicting someone drowning", types: ["hunt"] },
    { name: "Follow the directions on the Quaker loop's stone bench", types: ["hunt"] }
];
//moderate trail restrictions
bingoListMP[17] = [
    { name: "Don't run on any Southern Meadow trails", types: ["trail", "restriction"] },
    { name: "Don't run on any nature center trails", types: ["trail", "restriction"] },
    { name: "Don't run on any West Esker trails", types: ["trail", "restriction"] },
    { name: "Don't run on any nature center trails", types: ["trail", "restriction"] },
    { name: "Don't cross between the ponds", types: ["trail", "restriction"] }
];
//moderate category combos
bingoListMP[18] = [
    { name: "Visit both Canfield entrance signs", types: ["sign", "landmark"] },
    { name: "Visit all seven shelters", types: ["shelter", "landmark"] },
    { name: "Circle both water towers", types: ["water_tower", "landmark"] },
    { name: "Visit two permanent bathroom structures", types: ["minor", "landmark"] }
];
//road restrictions
bingoListMP[19] = [
    { name: "Don't cross Canfield Road", types: ["road", "restriction"] },
    { name: "Don't cross Douglas Road", types: ["road", "restriction"] },
    { name: "Don't cross Hopkins Point Road", types: ["road", "restriction"] },
    { name: "Don't cross Pond Road", types: ["road", "restriction"] }
];
//minor hill climbs
bingoListMP[20] = [
    { name: "Climb Echo Peak", types: ["trail", "achievement"] },
    { name: "Climb Fern Hill", types: ["trail", "achievement"] },
    { name: "Climb Eagle Hill", types: ["trail", "achievement"] }
];
//difficult trail completes
bingoListMP[21] = [
    { name: "Run the Pine Woods loop", types: ["pinewoods", "trail", "achievement"] },
    { name: "Run the Grasslands Trail loop", types: ["grasslands", "trail", "achievement"] },
    { name: "Run the North Meadow loop", types: ["northmeadow", "trail", "achievement"] },
    { name: "Run the Fern Valley loop", types: ["fernvalley", "trail", "achievement"] }
];
//off-trail landmarks
bingoListMP[22] = [
    { name: "Violate a 'one way do not enter' ski sign", types: ["off-trail", "landmark"] },
    { name: "Visit a makeshift shelter", types: ["off-trail", "landmark"] },
    { name: "Visit an old foundation or chimney", types: ["off-trail", "landmark"] },
    { name: "Visit a grave stone", types: ["off-trail", "landmark"] }
];
//intersections
bingoListMP[23] = [
    { name: "Visit intersection 1", types: ["intersection", "landmark"] },
    { name: "Visit intersection 12", types: ["intersection", "landmark"] },
    { name: "Visit intersection 17", types: ["intersection", "landmark"] },
    { name: "Visit intersection 26", types: ["intersection", "landmark"] },
    { name: "Visit intersection 30", types: ["intersection", "landmark"] },
    { name: "Visit intersection 33", types: ["intersection", "landmark"] }
];
//trail scavenger hunt 3
bingoListMP[24] = [
    { name: "Glimpse the Rochester skyline", types: ["hunt"] },
    { name: "Find a Heart Association Walk sign", types: ["hunt"] },
    { name: "Reach the finish clean", types: ["hunt"] },
    { name: "Reach the finish muddy", types: ["hunt"] }
];
//difficult trail restrictions
bingoListMP[25] = [
    { name: "Don't run on any East Esker trails", types: ["trail", "restriction"] },
    { name: "Don't run on any Quaker loop trails", types: ["trail", "restriction"] },
    { name: "Don't cross any roads", types: ["road", "restriction"] }
];

//RE4-OBJ (resident evil 4)
//chapter 1-1
bingoListRE4[1] = [
    { name: "Drop the Pearl Pendant in the shit", types: ["treasure", "ch1"] },
    { name: "Save the dog", types: ["ch1"] },
    { name: "Kill 2 Chainsaw Men in the village", types: ["enemies", "ch1"] },
    { name: "Kill 2 Ganados on the bridge", types: ["enemies", "ch1"] },
    { name: "Pick up the Yellow Herb in the village", types: ["yellowherb", "ch1"] },
    { name: "Shoot hand grenade crow", types: ["enemies", "ch1"] }
];
//chapter 1-2
bingoListRE4[2] = [
    { name: "Drop the Brass Pocket Watch in the shit", types: ["treasure", "ch1"] },
    { name: "Pick up the Yellow Herb in the valley", types: ["yellowherb", "ch1"] },
    { name: "Don't kill anything in Chapter 1-2", types: ["pacifist", "ch1"] }
];
//chapter 1-3
bingoListRE4[3] = [
    { name: "Kill bathroom Ganado", types: ["enemies", "ch1"] },
    { name: "Pick up the Yellow Herb by the lake", types: ["yellowherb", "ch1"] },
    { name: "Follow Mendez back into his bedroom", types: ["cutscene", "ch1"] },
    { name: "Shoot flash grenade crow", types: ["enemies", "ch1"] }
];
//chapter 2-1
bingoListRE4[4] = [
    { name: "Pick up the Yellow Herb in the village quarry", types: ["yellowherb", "ch2"] },
    { name: "Kill dogs across the lake", types: ["enemies", "ch2"] },
    { name: "Hit El Gigante with an egg", types: ["enemies", "weapons", "ch2"] }
];
//chapter 2-2
bingoListRE4[5] = [
    { name: "Pick up the Yellow Herb in the cabin", types: ["yellowherb", "ch2"] },
    { name: "Don't kill anything in Chapter 2-2", types: ["pacifist", "ch2"] },
    { name: "Kill infected merchant at the bridge to the cabin", types: ["merchant", "ch2"] }
];
//chapter 2-3
bingoListRE4[6] = [
    { name: "Only kill one Chainsaw Sister", types: ["enemies", "treasure", "ch2"] },
    { name: "Drop rocks on El Gigante", types: ["enemies", "ch2"] },
    { name: "Complete the Beerstein", types: ["treasure", "ch2"] },
    { name: "Hit Mendez with an egg", types: ["enemies", "weapons", "ch2"] },
    { name: "Kill infected merchant near the village exit", types: ["merchant", "ch2"] }
];
//easy restrictions
bingoListRE4[7] = [
    { name: "No health upgrades", types: ["restriction", "healing"] },
    { name: "Never upgrade attache case", types: ["restriction"] },
    { name: "No rocket launchers", types: ["restriction", "weapons"] },
    { name: "No tactical vest", types: ["restriction"] },
    { name: "No hiding Ashley in dumpsters", types: ["ashley", "restriction"] },
    { name: "Never sell items", types: ["restriction", "treasure"] }
];
//chapter 3-1
bingoListRE4[8] = [
    { name: "Pick up the Yellow Herb on the castle wall", types: ["yellowherb", "ch3"] },
    { name: "Pick up the Yellow Herb in the audience hall", types: ["yellowherb", "ch3"] },
    { name: "Hit the prison Garrador with an egg", types: ["enemies", "weapons", "ch3"] }
];
//chapter 3-2
bingoListRE4[9] = [
    { name: "Pick up the Yellow Herb in the Novistador prison cell", types: ["yellowherb", "ch3"] },
    { name: "Pick up the Yellow Herb in the garden", types: ["yellowherb", "ch3"] },
    { name: "Kill all ritual cultists in the castle temple", types: ["enemies", "ch3"] },
    { name: "Avoid spawning the gatling gun in the castle gallery", types: ["enemies", "ch3"] },
    { name: "Hit a Novistador with an egg", types: ["enemies", "weapons", "ch3"] }
];
//chapter 3-3
bingoListRE4[10] = [
    { name: "Pick up the Rocket Launcher", types: ["ch3"] },
    { name: "Kill the Garrador without leaving the cage", types: ["enemies", "ch3"] },
    { name: "Don't kill anything in Chapter 3-3", types: ["pacifist", "ch3"] }
];
//chapter 3-4
bingoListRE4[11] = [
    { name: "Pick up the Yellow Herb in the study", types: ["yellowherb", "ashley", "ch3"] },
    { name: "Do not kill either cultist in the study", types: ["enemies", "ashley", "ch3"] },
    { name: "Go down a ladder as Ashley", types: ["ashley", "ch3"] }
];
//chapter 4-1
bingoListRE4[12] = [
    { name: "Complete an Elegant Mask", types: ["treasure", "ch4"] },
    { name: "Collapse Novistador nest", types: ["enemies", "ch4"] },
    { name: "Pick up the Yellow Herb in the castle lobby", types: ["yellowherb", "ch4"] },
    { name: "Pick up the Yellow Herb in the old aqueduct", types: ["yellowherb", "ch4"] },
    { name: "Hit Verdugo with an egg", types: ["enemies", "weapons", "ch4"] },
    { name: "Pick up the Elegant Perfume Bottle", types: ["treasure", "ch4"] }
];
//chapter 4-2
bingoListRE4[13] = [
    { name: "Complete the Salazar Family Crown", types: ["treasure", "ch4"] },
    { name: "Don't kill anything in Chapter 4-2", types: ["pacifist", "ch4"] }
];
//chapter 4-3
bingoListRE4[14] = [
    { name: "Pick up the Yellow Herb in the castle mine", types: ["yellowherb", "ch4"] },
    { name: "Kill all 4 Chainsaw Men in the castle mine", types: ["enemies", "ch4"] },
    { name: "Pick up the Staff of Royalty", types: ["treasure", "ch4"] }
];
//chapter 4-4
bingoListRE4[15] = [
    { name: "Pick up the Yellow Herb in the castle tower", types: ["yellowherb", "ch4"] },
    { name: "Kill all Plagas beneath Salazar", types: ["enemies", "ch4"] },
    { name: "Hit Salazar with an egg", types: ["enemies", "weapons", "ch4"] }
];
//collection
bingoListRE4[16] = [
    { name: "Collect 30 Spinels", types: ["treasure", "ch1", "ch2", "ch3"] },
    { name: "Collect 20 Violet Blues", types: ["treasure", "ch3", "ch4"] },
    { name: "Collect 10 Emeralds", types: ["treasure", "ch5"] },
    { name: "Collect 3 Rubies", types: ["treasure", "ch1", "ch2", "ch4"] },
    { name: "Collect all bottlecaps", types: ["sidequest", "ch3", "ch4", "ch5"] },
    { name: "Hold 1,000,000 pesetas", types: ["treasure"] }
];
//deaths
bingoListRE4[17] = [
    { name: "Get munched by Del Lago while on land", types: ["enemies", "ch1"] },
    { name: "Get munched by Salazar", types: ["enemies", "ch4"] },
    { name: "Get decapitated by a chainsaw", types: ["enemies", "ch1", "ch2", "ch4"] },
    { name: "Get pulled into a pool of lava", types: ["enemies", "ch4"] },
    { name: "Get munched by a Plaga B", types: ["enemies", "ch3", "ch4", "ch5"] },
    { name: "Get killed in a cutscene", types: ["ch1", "ch2", "ch3", "ch4", "ch5"] },
    { name: "Get shot by Luis", types: ["ch2"] },
    { name: "Get crushed by a spiked ceiling", types: ["ch3", "ch4"] }
];
//chapter 5-1
bingoListRE4[18] = [
    { name: "Pick up the Infrared Scope", types: ["treasure", "ch5"] },
    { name: "Pick up the Yellow Herb in the monitor room", types: ["yellowherb", "ch5"] },
    { name: "Pick up the Yellow Herb in the waste treatment plant", types: ["yellowherb", "ch5"] },
    { name: "Pick up the Yellow Herb in the radio tower", types: ["yellowherb", "ch5"] },
    { name: "Kill infected merchant outside the compound", types: ["merchant", "ch5"] }
];
//chapter 5-2
bingoListRE4[19] = [
    { name: "Pick up the Yellow Herb in the Regenerador garbage room", types: ["yellowherb", "ch5"] },
    { name: "Ashley operates the wrecking ball", types: ["ashley", "ch5"] },
    { name: "Don't kill anything in Chapter 5-2", types: ["pacifist", "ch5"] },
    { name: "Kill infected merchant after the bulldozer crash", types: ["merchant", "ch5"] }
];
//chapter 5-3
bingoListRE4[20] = [
    { name: "Complete the Golden Lynx", types: ["treasure", "ch5"] },
    { name: "Pick up the Yellow Herb in the underground cavern", types: ["yellowherb", "ch5"] },
    { name: "Pick up the Yellow Herb at the campsite", types: ["yellowherb", "ch5"] },
    { name: "Pick up the Yellow Herb in the Krauser ruins", types: ["yellowherb", "ch5"] },
    { name: "Beat Krauser with only a knife", types: ["enemies", "knife", "ch5"] },
    { name: "Hit U-3 with an egg", types: ["enemies", "weapons", "ch5"] },
    { name: "Kill infected merchant under the campsite", types: ["merchant", "ch5"] }
];
//chapter 5-4
bingoListRE4[21] = [
    { name: "Pick up the Yellow Herb in the fort", types: ["yellowherb", "ch5"] },
    { name: "Pick up the Yellow Herb in the Regenerador prison", types: ["yellowherb", "ch5"] },
    { name: "Pick up the Yellow Herb in the outside passage", types: ["yellowherb", "ch5"] },
    { name: "Kill the unidentified thing in the Regenerador prison", types: ["enemies", "ch5"] }
];
//healing
bingoListRE4[22] = [
    { name: "Only heal using eggs", types: ["healing"] },
    { name: "Only heal using herbs", types: ["healing"] },
    { name: "Only heal using spray", types: ["healing"] },
    { name: "Only heal using fish", types: ["healing"] }
];
//weapons
bingoListRE4[23] = [
    { name: "Pistols only", types: ["weapons", "ch1"] },
    { name: "Shotguns only", types: ["weapons", "ch1"] },
    { name: "Rifles only", types: ["weapons", "ch1"] },
    { name: "TMP only", types: ["waepons", "ch1"] },
    { name: "Magnums only", types: ["weapons", "ch3"] },
    { name: "Mine Thrower only", types: ["weapons", "ch3"] }
];
//hard restrictions
bingoListRE4[24] = [
    { name: "No weapon upgrades", types: ["weapons", "restriction"] },
    { name: "No grenades", types: ["weapons", "restriction"] },
    { name: "No knife", types: ["weapons", "knife", "restriction"] },
    { name: "No ammo stored in attache", types: ["restriction"] }
];
//sidequests
bingoListRE4[25] = [
    { name: "Shoot all blue medallions on the farm", types: ["sidequest", "ch1"] },
    { name: "Shoot all blue medallions in the cemetery", types: ["sidequest", "ch1"] },
    { name: "Hit 25/25 in Shooting Range A", types: ["sidequest", "ch3"] },
    { name: "Hit 25/25 in Shooting Range B", types: ["sidequest", "ch4"] },
    { name: "Hit 25/25 in Shooting Range C", types: ["sidequest", "ch4"] },
    { name: "Hit 25/25 in Shooting Range D", types: ["sidequest", "ch5"] }
];

bingo();