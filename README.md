# speengo

Speedrunning bingo written in Java/HTML

## About

Speengo is a tool that randomly generates bingo cards for races. All participants are given the same bingo board and compete to see who can complete five objectives in a row the fastest.

This code is adapted from Narcissa Wright's srl bingo v5, trimmed down quite a bit and reworked to improve flexibility and customization.

## Usage

Open the HTML file in a browser, then click on a category to generate a random card. Once generated, the URL can be shared among race participants to ensure everyone has the same board.

## Create your own board

Each category is built as an array of 25 different items. These items are themselves arrays of objectives, formatted as objects. The generator will pick a random objective from each of these arrays to display on the board. Following the example of the provided categories is a good way to feel out how to add your own.

Adding a category starts with the objective list (objective-list.js). At the top of the file, define the variable that will hold your objective arrays with 'bingoList' followed by a category code or ID. Initialize this as an array.

Following that, add objectives to the new array by defining positions 1-25. Each set of objectives is defined by a name and a collection of types. The name is what will be shown on the bingo board. Types can be thought of as markers to keep similar objectives separate from each other, preventing uninterestingly easy boards from being created.

Once the objectives are complete, add a new case to the switch statement in the speengo.js file. Name the case the same as the category code you picked before, set the bingoList variable to the name of the variable defined in the objective list, and set catLen equal to the number of characters in the category code.

Finally, in the HTML file (speengo.html), add a link in the bingo categories section that calls the newCard function. Set the argument to the category code you've been using, and that should be that!

## Miles to go

Though speengo is pretty well tied off, there are a few things that still bug me. One thing is that the bingo board and categories are coded into the HTML statically, and I think it might be better to do it dynamically. The other thing is that while it doesn't look as bad as it could, it could still be styled into something a little nicer. Adding more boards is always a fun endeavor as well.